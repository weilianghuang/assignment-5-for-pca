function [Underlying, PCA_S, V, D, Proportion] = PCA_Result(Underlying, p)
%This fuction is to get the result of the analysis from underlyings
%Input 
% dataframe: Underlying
% how many eigvalues we want to save: p
%Output
% Underlying: what underlyings included
% PCA_S: PCA score
% V: eigvectors
% D: eigvalues
% Proportion: the proportion of each eigvalues
DATA = [];
for i = 1:length(Underlying)
    DATA = [DATA, GetLogreturn(char(Underlying(i)))];
end
[PCA_S, V, D, Proportion] = PCA_Caculate(DATA, p);
end

