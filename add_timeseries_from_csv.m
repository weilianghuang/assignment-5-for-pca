%This m-file is intended to add a set of timeseries data from a Yahoo!
%finance CSV download.  The inputs required are the ticker of the company
%and a CSV or XLS data file in Yahoo! finance format.
%Syntax: [ID]=add_timeseries_from_csv(ticker,XLS_filepath) 
function [ID]=add_timeseries_from_csv(ticker,csv_filepath)
ID=get_name_id(ticker);
if(isnumeric(ID)==1)
conn = database('Weiliang', '', '');
colnames = {'InstID';'Date';'OpenPrice';'HighPrice';'LowPrice';'ClosePrice ';'Volume'};
[num,txt,raw]=xlsread(csv_filepath);
for i = 1:length(num)
    coldata = {ID,txt(i+1,1),num(i,1),num(i,2),num(i,3),num(i,4),num(i,5)}; 
    fastinsert(conn,'MFM_Financial.FinData.HistPrice',colnames,coldata);
end
close(conn);
end
end
