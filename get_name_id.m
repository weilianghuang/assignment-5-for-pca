%This m-file is intended to get a company's ID from the%MFM_Financial.FinData.Instrument table.
%Syntax: [ID]=get_name_id(ticker)
function [ID]=get_name_id(ticker)
%Set preferences with setdbprefs
setdbprefs('DataReturnFormat', 'cellarray'); setdbprefs('NullNumberRead', 'NaN');
setdbprefs('NullStringRead', 'null');
conn = database('Weiliang', '', '');
curs = exec(conn, ['SELECT  Instrument.ID FROM MFM_Financial.FinData.Instrument where StockTicker=''', ticker,'''']);
curs = fetch(curs);
ID=cell2mat(curs.Data);
close(conn);
end