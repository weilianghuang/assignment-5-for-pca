%This m-file is intended to add a company name to the
%MFM_Financial.FinData.Instrument table.
%Syntax: add_name(company_name,ticker_symbol,market)
function add_name(company,ticker,market,sector)
conn = database('Weiliang', '', '');
colnames = {'CompanyName';'StockTicker';'Market';'Sector'};
coldata = {company,ticker,market,sector};
fastinsert(conn,'MFM_Financial.FinData.Instrument',colnames,coldata);
close(conn);
end