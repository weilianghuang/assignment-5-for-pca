%This m-file use for once
%%Get the MFM_Financial.FinData.Instrument
add_name('Microsoft','MSFT','NASDQA','Technology');
add_name('General Electric','GE','NYSE','Industrial Goods');
add_name('Google','GOOG','NASDQA','Technology');
add_name('Apple','AAPL','NASDQA','Technology');
add_name('Amazon','AMZN','NASDQA','Technology');
add_name('Facebook','FB','NASDQA','Technology');
add_name('The Coca-Cola Company','KO','NYSE','Consumer Goods');
add_name('Valero Energy Corporation','VLO','NYSE','Basic Materials');
%%Get the MFM_Financial.FinData.HistPrice
add_timeseries_from_csv('MSFT','MSFT.csv');
add_timeseries_from_csv('GE','GE.csv');
add_timeseries_from_csv('GOOG','GOOG.csv');
add_timeseries_from_csv('AAPL','AAPL.csv');
add_timeseries_from_csv('AMZN','AMZN.csv');
add_timeseries_from_csv('FB','FB.csv');
add_timeseries_from_csv('KO','KO.csv');
add_timeseries_from_csv('VLO','VLO.csv');
