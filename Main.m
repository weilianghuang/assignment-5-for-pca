% Analysis the two different group
%% The group with same Sector: Technology
SameSector = {'AAPL';'MSFT';'GOOG';'AMZN'};
for i = 1:4
[Underlying_1, PCA_S_1, V_1, D_1, Proportion_1] = PCA_Result(SameSector, i);
subplot(4,1,i);
titlename = strcat(num2str(i),' dimension return data');
plot(PCA_S_1,'*');
title(titlename)
end
Max_Proportion_1 = max(Proportion_1);
%% The group with different Sector
DifferentSector = {'AAPL';'GE';'KO';'VLO'};
for i = 1:4
[Underlying_2, PCA_S_2, V_2, D_2, Proportion_2] = PCA_Result(DifferentSector, i);
subplot(4,1,i);
titlename = strcat(num2str(i),' dimension return data');
plot(PCA_S_2,'*');
title(titlename)
end
Max_Proportion_2 = max(Proportion_2);