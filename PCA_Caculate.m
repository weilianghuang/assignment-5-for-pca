function [PCA_S, V, D, Proportion] = PCA_Caculate(Pmatrix, p)
%This fuction is to caculte the PCA score.
%Input 
% dataframe: Pmatrix
% how many eigvalues we want to save: p
%Output
% PCA_S: PCA score
% V: eigvectors
% D: eigvalues
% Proportion: the proportion of each eigvalues
[Row Col] = size(Pmatrix);
CovP = cov(Pmatrix);
[V D]=eigs(CovP);
MeanP = mean(Pmatrix);
[SD index] = sort(sum(D),'descend');
tempX = repmat(MeanP,Row,1);
SCORE = (Pmatrix-tempX)*V;
PCA_S = [];
Proportion = SD / sum(SD);
for i = 1 : p
    PCA_S = [PCA_S, SCORE(:,index(i))];
end
end

